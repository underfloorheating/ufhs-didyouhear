<?php

/**
 * Didyouhear controller
 *
 * @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Didyouhear_Adminhtml_OptionsController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction()
	{
		$this->_title($this->__('Options'));
		$this->loadLayout();
		$this->_initLayoutMessages('adminhtml/session');
		$this->_setActiveMenu('ufhs');

		return $this;
	}

	public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }
}