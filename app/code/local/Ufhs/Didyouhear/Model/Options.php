<?php

/**
 * 
 * @version     $Id$
 * @package     Ufhs_Didyouhear
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 * 
 */
class Ufhs_Didyouhear_Model_Options extends Mage_Core_Model_Abstract
{
	public function _construct()
    {
        parent::_construct();
        $this->_init('didyouhear/options');
    }

}