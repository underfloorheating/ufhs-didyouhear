<?php

/**
 * @version     $Id$
 * @package     Ufhs_Didyouhear
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Didyouhear_Model_Resource_Options extends Mage_Core_Model_Resource_Db_Abstract
{

    public function _construct()
    {
        $this->_init('didyouhear/options', 'id');
    }

}