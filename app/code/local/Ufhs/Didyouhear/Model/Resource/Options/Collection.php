<?php

class Ufhs_Didyouhear_Model_Resource_Options_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
 {
     public function _construct()
     {
         parent::_construct();
         $this->_init('didyouhear/options');
     }
}