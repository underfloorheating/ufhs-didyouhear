<?php

class Ufhs_Didyouhear_Model_Observer {
	public function writeData($event)
	{
		$optionsExists = Mage::getSingleton('core/resource')
		->getConnection('core_read')
		->isTableExists('didyouhear_options','redbox');

		if($optionsExists)
		{
			$data = Mage::app()->getRequest()->getPost('billing', array());
			$result = Mage::getModel('didyouhear/options')->load($data['howdidyouhear'], 'key')->getId();
			$value = $result ? $result : 0;
		}
		else 
		{
			$value = 0;
		}
		Mage::getSingleton('core/session')->setHear($value);		
	}

	public function readData($event)
	{
		$resultsExists = Mage::getSingleton('core/resource')
		->getConnection('core_read')
		->isTableExists('didyouhear_results','redbox');

		if($resultsExists)
		{
			$value = Mage::getSingleton('core/session')->getHear();
			$orderId = $event->getData()['order_ids'][0];
			$incrementId = Mage::getModel('sales/order')->load($orderId)->getData()['increment_id'];
			$total = Mage::getModel('sales/order')->load($orderId)->getData()['grand_total'];

			$db = Mage::getModel('didyouhear/results');
			$db->setData(array('value' => $value, 'order_id' => $orderId, 'increment_id' => $incrementId, 'total' => $total));
			$db->save();
		}
		
	}
}