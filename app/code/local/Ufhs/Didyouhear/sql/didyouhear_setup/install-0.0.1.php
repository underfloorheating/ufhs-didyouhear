<?php
/**
 * @version     $Id$
 * @package     Ufhs_Didyouhear
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */

$installer = $this;

$installer->startSetup();

$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('didyouhear/results')};

	CREATE TABLE {$installer->getTable('didyouhear/results')} (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`value` CHAR(50) NOT NULL DEFAULT '0',
	`order_id` CHAR(50) NOT NULL DEFAULT '0',
	`increment_id` CHAR(50) NOT NULL DEFAULT '0',
	`total` FLOAT NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
	)
	COLLATE='latin1_swedish_ci'
	ENGINE=InnoDB
	;

	DROP TABLE IF EXISTS {$installer->getTable('didyouhear/options')};

	CREATE TABLE {$installer->getTable('didyouhear/options')} (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`key` CHAR(50) NOT NULL DEFAULT '0',
	`value` CHAR(50) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
	)
	COLLATE='latin1_swedish_ci'
	ENGINE=InnoDB
	;
	");

$installer->endSetup();